import React, {Component} from "react";
import TodoList from "./TodoList.js";
import TodoAdd from "./TodoAdd.js";
import "../styles/App.css";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            counter: 0,

            todos: []
        }
        this.incrementCounter = this.incrementCounter.bind(this);
        this.addTodo = this.addTodo.bind(this);
    }

    incrementCounter(){
        this.setState({
            counter: this.state.counter+1
        })
    }

    addTodo(title) {
        let todos = this.state.todos;
        let maxId = 0;
        for(let todo of todos){
            if (todo.id > maxId){
                maxId = todo.id;
            }
        }
        todos.push({id: (maxId + 1), title: title});
        this.setState({
            todos: todos
        })
    }

    render(){
        return (
            <div className="App">
                <h1>Klick-Zähler und Todo-Liste mit React </h1>
                <h2>Klick-Zähler</h2>
                <h3>Der aktuelle Zählerstand: {this.state.counter}</h3>
                <button onClick={this.incrementCounter}> zähle hoch</button>
                <br/>
                <br/>
                <br/>
                <h2>Todo-Liste</h2>
                <TodoList todos={this.state.todos}   />
                <br/>
                <TodoAdd onAdd={this.addTodo} />
            </div>
        );
    }
}
export default App;