import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import TodoList from './components/TodoList';
import { createStore } from 'redux'
import { Provider } from 'react-redux';
import { incrementCounter } from './actions/index';
import reduce from './reducers/index';
import './styles/Panel.css';

let store = createStore(reduce);
console.log(store.getState());

ReactDOM.render(
    <Provider store={store}>
        <div>
            <App />
            <TodoList />
        </div>
    </Provider>,
    document.getElementById('root')
);