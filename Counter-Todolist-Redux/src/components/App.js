import React, { Component } from 'react';
import { incrementCounter } from '../actions/index';
import { connect } from 'react-redux';
import '../styles/App.css';

class App extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="App">
                <h1>Klick-Zähler und Todo-Liste mit Redux </h1>
                <h2>Klick-Zähler</h2>
                <h3>Der aktuelle Zählerstand: {this.props.value}</h3>
                <button onClick={this.props.onIncrement}>zähle hoch</button>
                <br /> <br /> <br />
            </div>
        );
    }
}

let mapStateToProps = function(state) {
    return {
        value: state.counter
    }
}

let mapDispatchToProps = {
    onIncrement: incrementCounter
}

let AppContainer = connect(mapStateToProps, mapDispatchToProps)(App);

export default AppContainer;