import React, { Component } from 'react';
import Panel from "./Panel";

class TodoAdd extends Component {

    constructor(props) {
        super(props);

        this.onTodoAdd = this.onTodoAdd.bind(this);
        this.onTodoInputChange = this.onTodoInputChange.bind(this);

        this.state = {
           newTodo: "Was muss getan werden?"
        }
    }

    onTodoAdd() {
        this.props.onAdd(this.state.newTodo);
    }

    onTodoInputChange(event) {
        this.setState({
            newTodo: event.target.value
        })
    }

    render() {
        return (
            <Panel title="Todo hinzufügen">
                <input
                    type="text"
                    onChange={this.onTodoInputChange}
                    value={this.state.newTodo}
                />

                <button
                    onClick={this.onTodoAdd}>
                    Todo hinzufügen
                </button>

            </Panel>
        )
    }
}

export default TodoAdd;